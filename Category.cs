﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace todolist
{
    public class Category
    {
        public bool isChecked { get; set; }

        public string _name;

        public string Name {
            get {
                return this._name;
            }
            set
            {
                this._name = value;
            }
        }
        public Category()
        {

        }

        public override string ToString () {
            return this.Name;
        }

        public Category(string name, bool ischecked = false)
        {
            this._name = name;
            isChecked = ischecked;
        }
    }
}
