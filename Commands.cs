﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace todolist
{
    public class Command
    {
        public static RoutedUICommand AddTask;
        public static RoutedUICommand RemoveTask;
        public static RoutedUICommand AddSubtask;
        public static RoutedUICommand RemoveSubtask;

        public static RoutedUICommand SaveTask;
        public static RoutedUICommand CancelTask;

        public static RoutedUICommand ManageCategories;
        public static RoutedUICommand EditTask;
        
        public static RoutedUICommand AddCategory;
        public static RoutedUICommand RemoveCategory;
        
        public static RoutedUICommand Print;
        public static RoutedUICommand ChangeColors;
        public static RoutedUICommand ManageStyles;

        static Command()
        {
            AddTask = new RoutedUICommand("Add Task", "Add", typeof(Command));
            RemoveTask = new RoutedUICommand("Remove Task", "Remove", typeof(Command));
            AddSubtask = new RoutedUICommand("Add Subtask", "Add", typeof(Command));
            RemoveSubtask = new RoutedUICommand("Remove Subtask", "Remove", typeof(Command));
            SaveTask = new RoutedUICommand("Save", "SaveTask", typeof(Command));
            CancelTask = new RoutedUICommand("Cancel", "CancelTask", typeof(Command));
            ManageCategories = new RoutedUICommand("Manage", "ManageCategories", typeof(Command));
            ManageStyles = new RoutedUICommand("ManageS", "ManageStyles", typeof(Command));
            EditTask = new RoutedUICommand("Edit", "EditTask", typeof(Command));
            AddCategory = new RoutedUICommand("AddCategory", "AddCategory", typeof(Command));
            RemoveCategory = new RoutedUICommand("RemoveCategory", "RemoveCategory", typeof(Command));
            Print = new RoutedUICommand("Print", "Print", typeof(Command));
            ChangeColors = new RoutedUICommand("ChangeColors", "ChangeColors", typeof(Command));
        }
    }
}
