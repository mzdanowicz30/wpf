﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;

namespace todolist {

    public enum SortType
    {
        Alphabetical,
        DateAsc,
        DateDesc,
        Priority
    }

    public partial class MainWindow : Window {


        SortType currentSortType = SortType.Alphabetical;
        string selectedCategory = null;
 
        public List<Task> AllTasks = new List<Task>();
        public List<Category> activeCategories = new List<Category>();
    
        public void ReSortData()
        {
            var list = (IEnumerable<Task>)AllTasks;
            if (list == null || ListBoksik == null)
                return;

            switch (currentSortType)
            {
                case SortType.Alphabetical:
                    list = list.OrderBy(t => t.Title);
                    break;
                case SortType.DateAsc:
                    list = list.OrderBy(t => t.StartTime);
                    break;
                case SortType.DateDesc:
                    list = list.OrderByDescending(t => t.StartTime);
                    break;
                case SortType.Priority:
                    list = list.OrderByDescending(t => t.Priority);
                    break;
            }
            
            list = list.Where(o => {
                if(o.Categories.Count == 0 && this.activeCategories.Count == 0) return true;
                else {
                    foreach (var cat in o.Categories) {
                        var matches = this.activeCategories.Where(c => c.Name == cat.Name);
                        if(matches.Count() > 0) return true;
                    }
                    return false;
                }
            });

            if (tbSearchQuery.Text != null);
            {
                if (tbSearchQuery.Text!="Search query")
                    list = list.Where(o => o.Title.ToLower().Contains(tbSearchQuery.Text.ToLower()));
                else
                    list = list.Where(o => o.Title.ToLower().Contains("".ToLower()));
            }

            var TaskList = new ObservableCollection<Task>(list);
            ListBoksik.ItemsSource = TaskList;
            ListBoksik.Items.Refresh();
        }

        public ObservableCollection<Category> categories = new ObservableCollection<Category>();

        public MainWindow() {
            
            InitializeComponent();
            DataContext = this;
            cbCategory.ItemsSource = categories;
            categories.Add(new Category("Życie"));
            categories.Add(new Category("Alkohol"));
            categories.Add(new Category("Szkoła"));
            categories.Add(new Category("Kochany WPF"));
            //categories.ToList().ForEach(c => this.activeCategories.Add(c));
            tbSearchQuery.GotFocus += this.RemoveText;
            tbSearchQuery.LostFocus += this.AddText;

            Task t = new Task()
            {
                Title = "Wyprowadzić psa",
                Description = "Wyprowadzić psa, pochodzić to tu, to tam.",
                Categories = new List<Category>() {
                }
            };

            t.subtasks = new List<Subtask>()
            {
                new Subtask("Subtask 1", t),
                new Subtask("Subtask 2", t),
                new Subtask("Subtask 3", t),
                new Subtask("Subtask 4", t)
            };

            Task t2 = new Task()
            {
                Title = "Zrobić projekt z WPF'a.",
                Description = "Został już tylko tydzień.",
            };

            AllTasks.Add(t);
            AllTasks.Add(t2);

            cbSort.SelectionChanged += CbSort_SelectionChanged;
            cbCategory.SelectionChanged += CbCategory_SelectionChanged;

            ReSortData();
        }

        private void CbCategory_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //var opt = (ComboBoxItem)this.cbCategory.SelectedValue;
            var x= cbCategory.Items;
            string opt = "Szkoła";
            if (opt.Equals("None"))
            {
                this.selectedCategory = null;
            }
            else
            {
                // this.selectedCategory = opt.Content.ToString();
                this.selectedCategory = opt;
            }

            ReSortData();
        }

        private void CbSort_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var opt = (ComboBoxItem)this.cbSort.SelectedValue;
            if (opt.Content.Equals("Date ascending"))
                currentSortType = SortType.DateAsc;

            if (opt.Content.Equals("Date descending"))
                currentSortType = SortType.DateDesc;

            if (opt.Content.Equals("Alphabetical"))
                currentSortType = SortType.Alphabetical;

            if (opt.Content.Equals("Priority"))
                currentSortType = SortType.Priority;

            ReSortData();
        }

        public void CanExecuteManageCategories(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        public void ManageCategoriesExecute(object sender, ExecutedRoutedEventArgs e)
        {
            ManageCategoriesWindow mcw = new ManageCategoriesWindow(this.categories);
            mcw.ShowDialog();
            this.categories.Clear();
            for (int i=0; i<mcw.categories.Count; i++)
                this.categories.Add(new Category(mcw.categories[i].Name));
        }

        public void CanExecuteManageStyles(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        public void ManageStylesExecute(object sender, ExecutedRoutedEventArgs e)
        {
            StyleWindow mcw = new StyleWindow();
            mcw.ShowDialog();
            
        }

        public void AddExecute(object sender, ExecutedRoutedEventArgs e)
        {
            TaskWindow newTask = new TaskWindow(tbAddTodo.Text,this.categories);
            newTask.ShowDialog();
            if (!newTask.canceled) {
                tbAddTodo.Text = "Add your todo";
                this.AllTasks.Add(newTask.task);
            }
            ReSortData();
        }
        public void RemoveExecute(object sender, ExecutedRoutedEventArgs e)
        {
            var t = (Task)e.Parameter;
            AllTasks.Remove(t);
            ReSortData();
        }


        public void RemoveSubtaskExecute(object sender, ExecutedRoutedEventArgs e)
        {
            var st = (Subtask)e.Parameter;

            st.Parent.subtasks.Remove(st);
            ReSortData();
        }

        public void EditTaskExecute(object sender, ExecutedRoutedEventArgs e)
        {
            var st = (Task)e.Parameter;
            int taskNumber = -1;
            for (int i=0; i<this.AllTasks.Count; i++)
                if (this.AllTasks[i].Equals(st))
                    taskNumber = i;
            TaskWindow newTask = new TaskWindow(st,this.categories);
            newTask.ShowDialog();
            if (newTask.canceled)
                this.AllTasks[taskNumber] = newTask.startTask.CopyTask();
            else
                this.AllTasks[taskNumber] = newTask.task.CopyTask();
            ReSortData();
        }

        public void CanExecuteAdd(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }
        public void CanExecuteRemove(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        

        private void SearchQuery_TextChanged(object sender, TextChangedEventArgs e)
        {   
            ReSortData();
        }

        private void CheckBox_Click (object sender,RoutedEventArgs e) {
            var category = (Category) (sender as CheckBox).DataContext;
            bool? isChecked = (sender as CheckBox).IsChecked;
            if (isChecked.Value)
                this.activeCategories.Add(category);
            else
                this.activeCategories.Remove(category);
            ReSortData();
        }

        public void RemoveText(object sender, EventArgs e)
        {
             tbSearchQuery.Text = "";
        }

        public void AddText(object sender, EventArgs e)
        {
             if(String.IsNullOrWhiteSpace(tbSearchQuery.Text))
                tbSearchQuery.Text = "Search query";
        }
        
        private void Print_Executed (object sender,ExecutedRoutedEventArgs e) {
            PrintDialog dialog = new PrintDialog();
            dialog.PrintVisual(this,"Task to do today!");
        }

        private void Print_CanExecute (object sender,CanExecuteRoutedEventArgs e) {
            e.CanExecute = true;
        }

    }

}