﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace todolist
{
    /// <summary>
    /// Interaction logic for ManageCategoriesWindow.xaml
    /// </summary>
    public partial class ManageCategoriesWindow : Window
    {

        public ObservableCollection<Category> categories;

        public string selectedCategory;

        public string SelectedCategory {
            get {
                return this.selectedCategory;
            }
            set {
                this.selectedCategory = value;
            }
        }

        public ManageCategoriesWindow()
        {
            InitializeComponent();
        }
        public ManageCategoriesWindow(ObservableCollection<Category> _categories)
        {
            InitializeComponent();
            DataContext = this;
            CategoryNameTB.DataContext = this;
            this.selectedCategory = "lol";
            this.categoriesList.ItemsSource = this.categories;
            this.categories = new ObservableCollection<Category>();
            this.categoriesList.Items.Clear();
            if (_categories != null)
            {
                foreach (Category cat in _categories)
                {
                    this.categories.Add(cat);
                    this.categoriesList.Items.Add(cat);
                }
            }
        }

        public void CanExecuteAddCategory(object sender, CanExecuteRoutedEventArgs e)
        {

            bool canExecute = true;
            if (this.categories != null) {
                for (int i = 0; i < this.categories.Count; i++) {
                    if (this.categories[i].Name == this.CategoryNameTB.Text)
                        canExecute = false;
                }
            }
            e.CanExecute = canExecute;
        }

        public void AddCategoryExecute(object sender, ExecutedRoutedEventArgs e)
        {
            this.categories.Add(new Category(this.CategoryNameTB.Text));
            this.categoriesList.Items.Add(new Category(this.CategoryNameTB.Text));
            this.CategoryNameTB.Text = "";
        }


        public void CanExecuteRemoveCategory(object sender, CanExecuteRoutedEventArgs e)
        {
            bool canExecute = false;
            if (this.categories != null)
                for (int i = 0; i < this.categories.Count; i++)
                    if (this.categories[i].Name == this.CategoryNameTB.Text)
                        canExecute = true;
            e.CanExecute = canExecute;
        }


        public void RemoveCategoryExecute(object sender, ExecutedRoutedEventArgs e)
        {
            for (int i = 0; i < this.categories.Count; i++)
                if (this.categories[i].Name == CategoryNameTB.Text)
                    this.categories.RemoveAt(i);
            this.categoriesList.Items.Clear();
            for (int i = 0; i < this.categories.Count; i++)
                this.categoriesList.Items.Add(new Category(this.categories[i].Name));
        }

        
    }
}
