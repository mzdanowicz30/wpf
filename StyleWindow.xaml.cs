﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace todolist
{
    /// <summary>
    /// Logika interakcji dla klasy StyleWindow.xaml
    /// </summary>
    public partial class StyleWindow : Window
    {
        private string colorChecked = null;
        private string fontChecked = "javanese";
        private int fontSize = 2;
        private string header;
        private string textfield;
        private string button;
        private string search;
        
        public StyleWindow()
        {
            InitializeComponent();
            
        }

        public StyleWindow(string color, string font, int Size)
        {
            InitializeComponent();
            colorChecked = color;
            fontChecked = font;
            fontSize = Size;
        }
        private void Style_Checked(object sender, RoutedEventArgs e)
        {
            FrameworkElement src = (FrameworkElement)sender;
            colorChecked = src.Tag.ToString();
        }

        private void Return_clicked(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Confirm_clicked(object sender, RoutedEventArgs e)
        {
            if(colorChecked != null)
            {
                ResourceDictionary newDictionary = new ResourceDictionary();
                newDictionary.Source = new Uri(colorChecked, UriKind.Relative);
                Application.Current.Resources.MergedDictionaries[0] = newDictionary;
            }
            this.Close();
        }
    }
}
