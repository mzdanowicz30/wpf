﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace todolist
{
    public class Subtask
    {
        public string Title { get; set; }
        public Task Parent { get; set; }

        public Subtask()
        {
            this.Title = "";
        }
        public Subtask(string title, Task parent)
        {
            this.Title = title;
            this.Parent = parent;
        }
    }
}
