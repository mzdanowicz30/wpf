﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace todolist {
    public class Task : INotifyPropertyChanged {

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string property)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;
            if(handler != null)
            {
                var e = new PropertyChangedEventArgs(property);
                handler(this, e);
            }
        }

        private string _title;
        public string Title {
            get {
                return this._title;
            }
            set
            {
                this._title = value;
                this.OnPropertyChanged("Title");
            }
        }

        private string _description;
        public string Description
        {
            get
            {
                return this._description;
            }
            set
            {
                this._description = value;
                this.OnPropertyChanged("Description");
            }
        }

        public List <Category> Categories { get; set; }

        private int _priority { get; set; }

        public int Priority 
        {
            get
            {
                return this._priority;
            }
            set
            {
                this._priority = value;
                this.OnPropertyChanged("Priority");
            }
        }

        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }

        public List<Subtask> subtasks{ get; set; }
        public int subtasksExist { get { return this.subtasks?.Count > 0 ? 1 : 0; } private set { } }

        public Task(){
            this.StartTime = DateTime.Now;
            this.Title = "Cos";
            this.Categories = new List<Category>();
        }


        public void addSubtask(string title)
        {
            if (this.subtasks == null)
                this.subtasks = new List<Subtask>();
            this.subtasks.Add(new Subtask(title, this));
        }

        public Task CopyTask() {
            Task newTask = new Task();
            newTask.Title = this.Title;
            newTask.Priority = this.Priority;
            newTask.StartTime = this.StartTime;
            newTask.EndTime = this.EndTime;
            newTask.Categories = this.Categories;
            newTask.Description = this.Description;
            newTask.subtasks = this.subtasks;
            newTask.subtasksExist = this.subtasksExist;
            newTask.Categories = this.Categories.ToList();
            return newTask;
        }

    }
}
