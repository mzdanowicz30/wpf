﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace todolist
{
    /// <summary>
    /// Interaction logic for TaskWindow.xaml
    /// </summary>
    public partial class TaskWindow : Window
    {

        public ObservableCollection<Category> categories = new ObservableCollection<Category>();

        public Task startTask;
        public Task task;
        public bool canceled = true;
        public ObservableCollection<Subtask> subtasks = new ObservableCollection<Subtask>();
        public TaskWindow()
        {
            InitializeComponent();
            this.startTask = new Task();
            this.task = new Task();
            this.DataContext = this.task;
            this.subtasksList.ItemsSource = this.subtasks;
        }

        public TaskWindow(string taskTitle, ObservableCollection<Category> _categories)
        {
            InitializeComponent();
            CategoriesComboBox.ItemsSource = categories;
            for (int i=0; i<_categories.Count; i++)
                this.categories.Add(new Category(_categories[i].Name));
            this.task = new Task();
            this.startTask = this.task.CopyTask();
            this.DataContext = this.task;
            this.task.Title = taskTitle;
            this.subtasksList.ItemsSource = this.subtasks;
        }

        private bool hasChecked(string name)
        {
            return task.Categories.SingleOrDefault(c => c.Name == name) != null;
        }

        public TaskWindow(Task task, ObservableCollection<Category> _categories)
        {
            InitializeComponent();
            CategoriesComboBox.ItemsSource = categories;
            this.task = task;


            for (int i=0; i<_categories.Count; i++)
                this.categories.Add(new Category(_categories[i].Name, hasChecked(_categories[i].Name)));

            this.startTask = this.task.CopyTask();
            this.DataContext = this.task;
            if (this.task.subtasks != null)
            {
                this.task.subtasks.ForEach(st =>
                {
                    this.subtasks.Add(st);
                });
            }
            this.subtasksList.ItemsSource = this.subtasks;
        }

        private void AddSubtaskCommand_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            this.subtasks.Add(new Subtask(e.Parameter.ToString(), task));
            this.task.subtasks = this.subtasks.ToList();

            newSubtaskTitle.Clear();
        }

        private void RemoveSubtaskCommand_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (subtasksList.SelectedIndex == -1)
                return;
            this.subtasks.RemoveAt(subtasksList.SelectedIndex);
        }

        private void RemoveSubtask(object sender, RoutedEventArgs e) {
            if (subtasksList.SelectedIndex == -1)
                return;
            int number = subtasksList.SelectedIndex;
            this.subtasks.RemoveAt(number);
            this.task.subtasks.RemoveAt(number);
        }

        private void AddSubtaskCommand_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = e.Parameter != null ? !string.IsNullOrWhiteSpace(e.Parameter.ToString()) : false;
        }

        private void RemoveSubtaskCommand_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = false;
        }


        private void SaveTaskCommand_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            this.canceled = false;
            this.Close();
        }

        private void SaveTaskCommand_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            if (this.task != null)
                e.CanExecute = !String.IsNullOrWhiteSpace(this.task.Title);
        }

        
        private void CancelTaskCommand_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            this.canceled = true;
            this.Close();
        }

        private void CancelTaskCommand_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void DateChanged(object sender, SelectionChangedEventArgs e) {
            var picker = sender as DatePicker;
            DateTime? date = picker.SelectedDate;
            if (date == null) {
                return;
            }
            else {
                DateTime orderDate = this.task.StartTime;
                if (orderDate < DateTime.Now) {
                    this.task.StartTime = DateTime.Now;
                }
            }
        }

        private void CategoryCheckBoxClick (object sender,RoutedEventArgs e) {
            var category = (Category) (sender as CheckBox).DataContext;
            bool? isChecked = (sender as CheckBox).IsChecked;
            if (isChecked.Value)
                this.task.Categories.Add(category);
            else
                this.task.Categories.Remove(category);
        }

    }
}
